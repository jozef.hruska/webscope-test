import { getArticlesByAuthorId, getTeamMembers, getAuthorNameById } from './main';

test('getArticlesByAuthorId: Author with an article.', () => {
	expect(getArticlesByAuthorId(1)).toEqual([{"id": 33, "text": "RxJS and redux-observable"}]);
});

test('getArticlesByAuthorId: Author without an article.', () => {
	expect(getArticlesByAuthorId(4)).toEqual([]);
});

test('getTeamMembers: Team with some members.', () => {
	expect(getTeamMembers({ id: 1, name: 'Webscope 1', members: [1, 2, 3, 4] })).toEqual([1, 2, 3, 4]);
});

test('getTeamMembers: Non-existing team ID.', () => {
	expect(getTeamMembers(4)).toEqual([]);
});

test('getAuthorNameById: ID with a existing name.', () => {
	expect(getAuthorNameById(1)).toEqual('Oliver');
});

test('getAuthorNameById: ID with a non-existing name.', () => {
	expect(getAuthorNameById(50)).toEqual(false);
});