import { coolAdd } from './main';

test('firstToUpper: Simple addition - Two parameters.', () => {
	expect(coolAdd(2, 2)).toEqual(4);
});

test('firstToUpper: Simple addition - Single parameter.', () => {
	expect(coolAdd(2)(2)).toEqual(4);
});