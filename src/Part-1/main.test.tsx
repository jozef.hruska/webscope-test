import { firstToUpper } from './main';

test('firstToUpper: Empty string value.', () => {
	expect(() => firstToUpper('')).toThrow(Error);
});

test('firstToUpper: First char lowercase.', () => {
	expect(firstToUpper('sss')).toEqual('Sss');
});

test('firstToUpper: First char uppercase.', () => {
	expect(firstToUpper('Sss')).toEqual('Sss');
});

test('firstToUpper: First char numeric.', () => {
	expect(firstToUpper('1ss')).toEqual('1ss');
});