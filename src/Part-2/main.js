function test() {
  console.info('Double clicked')
}

/* Element */
let button = document.querySelector('#button');

/* Simple click event */
let simpleClick$ = Rx.Observable.fromEvent(button, 'click');

/* Double click event */
let doubleClick$ = simpleClick$
	.buffer(simpleClick$.debounceTime(250))
	.map(actions => actions.length)
	.filter(count => count === 2)
	.subscribe(() => test());