/**
 * Method converts first character of string to uppercase.
 * @param {string} sourceString String of which frist char will be converted to uppercase
 * @returns {string} String with first uppercase character
 */
export function firstToUpper(sourceString: string) : string {
	let firstCharacter = null;

	if (sourceString[0]) firstCharacter = sourceString[0].toUpperCase();
	else throw new Error('Invalid string value.')

	return (firstCharacter + sourceString.slice(1, sourceString.length));
}