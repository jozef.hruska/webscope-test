/**
 * Variable return function performing simple addition of two parameters.
 * @param a First addend
 * @param b Second addend
 * @returns {function|number} Returns sum or a function expecting second addend.
 */
export function coolAdd(a: number, b?: number) : any {
	if (!b) {
		return function(b: number) : number {
			return a + b;
		};
	} else return a + b;
}