const R = require('rambda');

/* Data */
export const articles = [
	{ id: 1, text: 'MobX in practise', authors: [3] },
	{
		id: 33, text: 'RxJS and redux-observable', authors: [1, 2, 3, 5, 6,
			7]
	},
	{ id: 23, text: 'Firebase', authors: [7, 2, 3] },
	{ id: 333, text: 'Really cool article' },
	{ id: 1234, text: 'Ramda.js and Redux combined', authors: [2] },
	{ id: 2, text: 'CSS in JS', authors: [3, 5] },
]
export const authors = [
	{ id: 1, name: 'Oliver' },
	{ id: 2, name: 'Jan' },
	{ id: 3, name: 'Jakub' },
	{ id: 4, name: 'Peter' },
	{ id: 5, name: 'Tomas' },
	{ id: 6, name: 'Drahoslav' },
	{ id: 7, name: 'Honza' },
]
export const teams = [
	{ id: 1, name: 'Webscope 1', members: [1, 2, 3, 4] },
	{ id: 2, name: 'Webscope 2', members: [5, 6, 7] },
]

/**
 * Gets all articles that specified author co-authored.
 * @param authorId ID of an author of which to get his articles
 * @returns {object[]} Array of articles written by specific author.
 */
export function getArticlesByAuthorId(authorId: number): any[] {
	var result: any[] = [];
	let filteredArticles = R.pipe(
		R.filter(R.compose(R.any(R.contains(authorId)), R.values))
	)(articles);

	filteredArticles.forEach((article: any) => {
		let customArticle = {
			'id': article['id'],
			'text': article['text']
		}

		result.push(customArticle);
	});

	return result;
}

/**
 * Gets team members of a team.
 * @param team Object of a team to get it's members
 * @returns {number[]} Array of member IDs.
 */
export function getTeamMembers(team: any): number[] {
	return R.prop('members', team) ? R.prop('members', team) : [];
}

/**
 * Gets the name of an author specified by his ID.
 * @param authorId ID of an author for which to get the name
 * @returns {string} Name of an requested author.
 */
export function getAuthorNameById(authorId: number): string|boolean {
	let author = R.pipe(R.find(R.propEq('id', authorId)))(authors);
	return (author && author['name']) ? author['name'] : false;
}

/**
 * Returns a list of articles that are written by each member of given team.
 * @param {number} teamId ID of a team for which to print out articles
 */
function getTeamArticlesDescription(teamId: number): string {
	var result = '';

	let requiredTeam = R.pipe(R.find(R.propEq('id', teamId)))(teams);
	let teamMembers = getTeamMembers(requiredTeam);
	var allArticlesByTeam: any[] = [];

	/* Loop through each member and print out their info */
	teamMembers.forEach((teamMemberId: number) => {
		let writtenArticles = getArticlesByAuthorId(teamMemberId);
		let authorName = getAuthorNameById(teamMemberId);

		/* Check if author name does exist in DB */
		if (!authorName) throw new Error(`Author name for id ${teamMemberId} does not exist!`);

		writtenArticles.forEach((article: any) => {
			allArticlesByTeam.push(article);
		});

		switch (writtenArticles.length) {
			case 0:
				result = result.concat(`${authorName} wrote 0 articles.\n`);
				break;
			case 1:
				result = result.concat(`${authorName} wrote an article \'${writtenArticles[0]['text']}\'.\n`);
				break;
			case 2:
				result = result.concat(`${authorName} wrote articles \'${writtenArticles[0]['text']}\' and \'${writtenArticles[1]['text']}\'.\n`);
				break;
			default:
				result = result.concat(`${authorName} wrote articles \'${writtenArticles[0]['text']}\', \'${writtenArticles[1]['text']}\' and ${writtenArticles.length - 2} more.\n`);
		}
	});

	result = result.concat('-----\n');
	result = result.concat(`Team \'${requiredTeam['name']}\' co-authored ${R.uniq(allArticlesByTeam).length} out of ${articles.length} articles.\n`);
	return result;
}